    - README -

Program is compiled with "javac *.java"
Runs with "java ProjectPlanner <inputFile>"  (Main is in ProjectPlanner.java)

Assumptions: All task ids are > 0

The only part of my code influenced by some source is the searchForCycle() in TaskScheduler.java.
(Influenced LøkkeLet slide 26 @ https://www.uio.no/studier/emner/matnat/ifi/IN2010/h18/lysark/forelesning4.pdf)
The rest is my own implementations, so im sure there's room for performance improvements.