import java.util.*;

class TaskScheduler {
    private Task[] tasks;

    // Minimum time it takes to complete all tasks
    static int optimalCompleteTime;

    // Initializes the scheduler.
    // Proceeds to create schedule, if the project is realizable.
    TaskScheduler(Task[] tasks){
        this.tasks = tasks;
        checkRealizability();
        findOptimalSchedule();
    }

    // Checks projects realizability, by looking for a cycle in the tasks dependencies.
    private void checkRealizability(){
        for (Task t : tasks) {
            if (t.status == Status.UNVISITED)
                searchForCycle(t);
        }
        resetTasksStatus();
    }

    // Used in searchForCycle to keep track of the traversal in the graph.
    private Stack<Task> pathSoFar = new Stack<>();

    // TODO give credit to slide for algorithm
    // Goes through all tasks, looking for dependency cycles.
    // Terminates application, if a cycle is found.
    private void searchForCycle(Task t){
        if (t.status == Status.VISITING){
            // Cycle found
            System.out.println("Cycle found, at " + t.name);

            boolean cycleStarted = false;
            // Goes through path, printing out tasks from the cycle's start
            for (Task task : pathSoFar) {
                if (task == t) cycleStarted = true;
                if (cycleStarted) System.out.printf("(%d) %s -> ", task.id, task.name);
            }

            System.out.printf("(%d) %s\n Project not realizable\n\n", t.id, t.name);
            System.exit(1);
        }
        else if (t.status == Status.UNVISITED){
            // Cycle not found, continues search
            t.status = Status.VISITING;
            pathSoFar.add(t);
            for (Task neighbour : t.outEdges){
                searchForCycle(neighbour);
            }
            pathSoFar.pop();
            t.status = Status.VISITED;
        }
    }


    // Finds the tasks earliest and latest starts.
    private void findOptimalSchedule(){
        findEarliestStarts();
        findLatestStarts();
    }

    // Finds the tasks earliest start.
    private void findEarliestStarts(){
        for (Task t : tasks) {
            if (t.status == Status.UNVISITED)
                t.updateEarliestStart(null);
        }
        resetTasksStatus();
    }

    // Finds the tasks latest start.
    private void findLatestStarts(){
        for (Task t : tasks) {
            if (t.status == Status.UNVISITED) {
                t.updateLatestStart(optimalCompleteTime);
            }
        }
        resetTasksStatus();
    }

    // Resets all traversal flags in all tasks
    private void resetTasksStatus(){
        for (Task t : tasks) t.status = Status.UNVISITED;
    }

    // Creates and prints the schedule
    void printSchedule() {
        /* PriorityQueue and TreeSet is used for optimization reasons,
        * although, i could not find a good way to remove a task from
        * the queue, when it was completed. */

        Queue<Task> queue = new PriorityQueue<>();
        queue.addAll(Arrays.asList(tasks));
        int staff = 0;
        // Set of all times to check tasks to start/finish.
        TreeSet<Integer> finishingTimes = new TreeSet<>();
        finishingTimes.add(0);

        // Runs until all tasks started and finished.
        while (!finishingTimes.isEmpty()) {
            // Gets the next time an event occurs.
            int time = finishingTimes.pollFirst();
            StringBuilder sb = new StringBuilder();
            // Checks every task in the queue, if anything happens to it, at this time.
            for (Task t : queue){
                if (time == t.earliestStart){
                    // Task starts at this time.
                    sb.append(String.format("\t\t\t%s\n", "Starting: " + t.name));
                    finishingTimes.add(t.earliestStart+t.time);
                    staff += t.staff;
                }
                if (time == t.earliestStart + t.time){
                    // Task finishes at this time.
                    sb.insert(0, String.format("\t\t\t%s\n", "Finished: " + t.name));
                    staff -= t.staff;
                }
            }
            sb.append(String.format("\t\t\t%s", "Current staff: " + staff));

            // Prints all tasks started/finished at this time
            System.out.println("Time: " + time);
            System.out.println(sb);
        }
        System.out.printf("\n**** Shortest possible project execution is %d ****\n", optimalCompleteTime);
    }

    // Prints out relevant information about all the tasks.
    void printTasks() {
        System.out.println("\nList of tasks");
        for (Task t : tasks){
            System.out.printf("(%d) %s%s\nTime to complete: %d, staff required: %d\nTasks depending on this: ",
                    t.id, t.name, (t.isCritical() ? " (Critical)" : ""), t.time, t.staff);

            StringJoiner sj = new StringJoiner(", ");
            for (Task out : t.outEdges) sj.add(String.valueOf(out.id));

            System.out.println(sj.toString().isEmpty() ? "none" : sj);

            System.out.printf("Earliest start: %d, Latest start: %d, Slack: %d\n\n",
                    t.earliestStart, t.latestStart, t.slack());
        }
    }
}