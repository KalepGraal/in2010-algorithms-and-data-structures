import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class TaskCreator {

    /*
    * Creates tasks, and returns them in a Task array
    * */
    static Task[] createTasks(String filepath) {
        Task[] tasks = null;
        try (Scanner sc = new Scanner(new File(filepath))) {
            int numTasks = sc.nextInt();

            tasks = new Task[numTasks];
            List<List<Integer>> dependencies = new ArrayList<>(numTasks);

            // Creates tasks objects, puts them in tasks array
            instantiateTasks(sc, tasks, dependencies);

        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
            System.exit(1);
        } catch (NoSuchElementException ex) {
            System.out.println("File does not follow formatting");
            System.exit(1);
        }
        return tasks;
    }

    private static void instantiateTasks(Scanner sc, Task[] tasks,
                        List<List<Integer>> dependencies) throws NoSuchElementException {
        // Reads data from the file, creates tasks from this data
        for (int i = 0; i < tasks.length; i++) {
            int id = sc.nextInt();  String name = sc.next();
            int time = sc.nextInt(); int staff = sc.nextInt();

            // Reads dependencies from file, added to Task objects in createEdges()
            dependencies.add(createDependencyList(sc));

            tasks[i] = new Task(id,name,time,staff);
        }

        // Creates dependency-edges between tasks
        createEdges(tasks, dependencies);
    }

    private static List<Integer> createDependencyList (Scanner sc) {
        // Creates a list of ints from current scanner line, until a 0 is found.
        List<Integer> dependencies = new ArrayList<>();
        int nextInt = sc.nextInt();
        while (nextInt != 0) {
            dependencies.add(nextInt);
            // Assumes 0 always shows up
            nextInt = sc.nextInt();
        }
        return dependencies;
    }

    private static void createEdges(Task[] tasks, List<List<Integer>> dependencies){
        // Adds the dependencies to the task objects themselves.
        for (int i = 0; i < dependencies.size(); i++) {
            for (int j = 0; j < dependencies.get(i).size(); j++) {
                int depId = dependencies.get(i).get(j);
                tasks[depId - 1].outEdges.add(tasks[i]);  // -1 as ids starts at 1
            }
        }
    }
}