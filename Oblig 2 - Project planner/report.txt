    1. What is the time complexity of your solution? You must explain the stated complexity.
    If you have a different complexity for each problem, you need to discuss them separately.

The time complexity of the creation of the Task objects, is O(|V|*|E|),
where V is the vertices (Tasks), and E is the edges (dependencies).
The creation of the tasks can therefore be created in linear time.

When the program checks the projects realizability, it utilizes a depth first search,
which also has the time complexity of O(|V|*|E|). A DFS is also used when checking
the earliest and latest start for each task. After these searches, the program goes
through all tasks, and resets their status to unvisited, which brings the complexity
to O(|V| + |V|*|E|), which is still linear time.

The printout of the schedule, ended up having the complexity O(|T| * |V|), where T is
the times tasks start or complete. This complexity can in a worst case scenario be quadratic.
I tried to come up with a solution, where i would stop checking tasks already completed,
but i couldn't come up with a way to do so efficiently.

The printing of the tasks are done in linear time. O(|V|*|E|).

This unfortunately means that in a worst case scenario, the programs runtime will be
quadratic, unless i somehow could get the schedule printer to run in linear or better time.
If i could do that, the programs runtime would be linear.

    2. What requirements are there for the input graph in order for the project to be
    successfully planned? Please put the discussion in the light of graph theoretical properties.

The graph needs to be a directed acyclic graph (DAG). Which means all edges have a direction,
and there are no cycles. The graph does not need to be connected.

    3. What kind of graph algorithms did you use in your implementation?

Depth first searches were used.
