import java.util.ArrayList;
import java.util.List;

// Used in place of a boolean, mainly to look for cycles
enum Status {UNVISITED, VISITING, VISITED}

// Implements Comparable<Task> to be able to use PriorityQueue and TreeSet.
class Task implements Comparable<Task> {

    int 		id, time, staff;
    int         earliestStart, latestStart;

    String 		name;

    // Flag used in traversals
    Status      status = Status.UNVISITED;

    List<Task>  outEdges = new ArrayList<>();

    // Initializes tasks basic values. Start times are not assigned.
    Task (int id, String name, int time, int staff) {
        this.id = id;       this.name = name;
        this.time = time;   this.staff = staff;
    }

    // Methods for convenience
    int slack() { return latestStart - earliestStart; }
    boolean isCritical() { return slack() == 0; }
    private int earliestComplete() {return earliestStart + time; }

    // Overridden, to implement Comparable<Task>
    @Override
    public int compareTo(Task t) { return Integer.compare(earliestStart, t.earliestStart); }

    // Recursive method, that assigns the earliest possible start time,
    // to this task, and tasks depending on this task.
    void updateEarliestStart(Task prevTask) {
        if (prevTask == null){
            // Task has no dependencies
            earliestStart = 0;
        } else if (status == Status.UNVISITED ||
                prevTask.earliestComplete() > this.earliestStart){
            // Task
            earliestStart = prevTask.earliestStart + prevTask.time;
            updateProjectCompleteTime(earliestComplete());
        }
        status = Status.VISITED;

        for (Task t : outEdges) t.updateEarliestStart(this);
    }

    // Updates the optimalCompleteTime, if this tasks earliest complete time is
    // longer than the currently longest path.
    private void updateProjectCompleteTime(int n){
        if (TaskScheduler.optimalCompleteTime < n)
            TaskScheduler.optimalCompleteTime = n;
    }

    // Recursive method, that assigns the latest possible    start time,
    // to this task, and tasks depending on this task.
    int updateLatestStart(int optimalCompleteTime){
        // Base case, remains if there are no tasks depending on this task.
        latestStart = optimalCompleteTime - time;

        // Goes through each task depending on this task, and checks
        // if task needs to start earlier than the current latest start.
        for (Task t : outEdges) {
            int tmp = t.updateLatestStart(optimalCompleteTime) - time;
            if (tmp < latestStart)
                latestStart = tmp;
        }

        // Flags task as visited, and returns the new latest start.
        status = Status.VISITED;
        return latestStart;
    }

}