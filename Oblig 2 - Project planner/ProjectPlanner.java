public class ProjectPlanner {

    public static void main(String[] args) {
        System.out.printf("Result of running file: \"%s\"\n\n",args[0]);

        Task[] tasks = TaskCreator.createTasks(args[0]);

        TaskScheduler taskScheduler = new TaskScheduler(tasks);
        taskScheduler.printSchedule();
        taskScheduler.printTasks();
    }

}