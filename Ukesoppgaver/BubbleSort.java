import java.util.ArrayList;
import java.util.Arrays;

public class BubbleSort {

    ArrayList<Integer> aasdf = new ArrayList<>();

    public static void main(String[] args) {
        /*
        100    = 0ms
        1000   = 10ms
        10000  = 142ms
        20000  = 524ms
        40000  = 1995ms
        100000 = 11036ms
         */



        int[] arr = new int[40000];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr.length-1-i;
        }

        test(arr);
        /*test(new int[]{4,2,1,3,6,3,7,8,95,4,6,7,5,6,3,7,9,6,4,3,6});
        test(new int[]{1,2,3,4});
        test(new int[]{4,3,2,1});
        test(new int[]{1,4,3,2});
        test(new int[]{3,2,1,4});*/
    }

    private static void test(int[] arr){
        //System.out.println("før:   " + Arrays.toString(arr));

        long t = System.currentTimeMillis(); // nanosek
        arr = bubbleSort(arr);
        double tid = System.currentTimeMillis()-t; // millisek

        //System.out.println("etter: " + Arrays.toString(arr));
        System.out.printf("time: %.2fms\n", tid);
    }

    private static int[] bubbleSort(int[] A){
        for (int i = 0; i <= A.length - 2; i++) {
            for (int j = A.length - 1; j >= i + 1; j--) {
                if (A[j-1] > A[j]) {
                    // swap A[j-1] > A[j]
                    int tmp = A[j-1];
                    A[j-1] = A[j];
                    A[j] = tmp;
                }
            }
        }
        return A;
    }
}