public class test {
    /* Oppgave 7
     * L = {ListNode(), ListNode ... }
     * {3, 2, 6, 8}
     * */
    public int[] calculateIndegrees(ListNode[] L){
        int[] ret = new int[L.length];
        for (int i = 0; i < L.length; i++){
            for (ListNode ln : L) {
                if(L[i] == ln.next) ret[i]++;
            }
        }
        return ret;
    }

    class ListNode {
        public int destination;
        public ListNode next;
    }
}