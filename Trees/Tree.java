public interface Tree {
    void insert(int num);
    void insert(int ... num);
    int min();
    boolean search(int num);
    void remove(int num);
}