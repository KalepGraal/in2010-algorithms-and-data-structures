class Node implements Tree {
    Node right;
    Node left;
    private int value;

    Node(int value) {
        this.value = value;
    }

    public void insert(int ... num){
        for(int i : num) insert(i);
    }

    public void insert(int num){
        if (num < value) {
            if (left != null) left.insert(num);
            else left = new Node(num);
        }
        else if (num > value) {
            if (right != null) right.insert(num);
            else right = new Node(num);
        }
    }

    public boolean search(int num){
        if (value == num) return true;
        if (num < value) return left != null && left.search(num);
        return right != null && right.search(num);
    }

    public int min(){
        if(left != null) return left.min();
        return value;
    }

    private int max(){
        if(right != null) return right.min();
        return value;
    }

    private void remove(int num, Node prevNode){
        // System.out.println("@" + value + " prev: " + prevNode.value);
        if(num == value) {
            if(right != null){
                value = right.min();
                right.remove(value, this);
            } else if (left != null){
                value = left.max();
                left.remove(value, this);
            } else {
                if (prevNode.right == this) {
                    prevNode.right = null;
                    //System.out.println("Removed this node: " + value);
                }
                else if (prevNode.left == this) {
                    prevNode.left = null;
                    //System.out.println("Removed this node: " + value);
                }
            }
        }
        else if(num > value && right != null) right.remove(num, this);
        else if(num < value && left != null) left.remove(num, this);
    }

    public void remove(int num) {
        remove(num, this);
    }

    public StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
        if(right!=null) {
            right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
        }
        sb.append(prefix).append(isTail ? "└── " : "┌── ").append(value).append("\n");
        if(left!=null) {
            left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
        }
        return sb;
    }

    @Override
    public String toString() {
        return this.toString(new StringBuilder(), true, new StringBuilder()).toString();
    }

}
