public class BinaryTree {

    int number(BinNode t){
        //if (t == null) return 0;
        int num = 0;
        if(t.left != null) num += number(t.left);
        if(t.right != null) num += number(t.right);
        return num + 1;
    }

    int sum(BinNode t){
        //if (t == null) return 0;
        int sum = 0;
        if(t.left != null) sum += number(t.left);
        if(t.right != null) sum += number(t.right);
        return sum + t.data;
    }


    private class BinNode {
        int data;
        BinNode left;
        BinNode right;

        int number(){
            int num = 0;
            if(left != null) num += left.number();
            if(right != null) num += right.number();
            return num + 1;
        }

        int sum(){
            int sum = 0;
            if(left != null) sum += left.sum();
            if(right != null) sum += right.sum();
            return sum + data;
        }
    }
}