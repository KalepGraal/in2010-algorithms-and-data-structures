public class BinarySearchTree1 implements Tree {
    Node root;

    public void insert(int ... num){
        for(int i : num) insert(i);
    }

    public void insert(int num){
        if(root == null) root = new Node(num);
        else insertUnder(root, num);
    }

    private void insertUnder(Node n, int num){
        // antar man ikke gjør noe dersom tallet finnes i treet.
        if(num < n.value) {
            if(n.left == null) n.left = new Node(num);
            else insertUnder(n.left, num);
        } else if (num > n.value) {
            if(n.right == null) n.right = new Node(num);
            else insertUnder(n.right, num);
        }
    }

    public boolean search(int num){
        Node n = root;
        while(n != null){
            if(num == n.value) return true;
            if(num < n.value) n = n.left;
            else n = n.right;
        }
        return false;
    }

    public int min(){
        Node n = root;
        while (n.left != null){
            n = n.left;
        }
        return n.value;
    }

    public void remove(int num){
        Node node = root;
        if(node.value == num){
            if(node.right != null) findAndRemoveLowest(node.right);
            else if (node.left != null) findAndRemoveHighest(node.left);
        }
    }

    private int findAndRemoveLowest(Node node){

    }

    private int findAndRemoveHighest(Node node){

    }

    private class Node {
        Node right;
        Node left;
        int value;

        Node(int value){
            this.value = value;
        }


        public StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
            if(right!=null) {
                right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
            }
            sb.append(prefix).append(isTail ? "└── " : "┌── ").append(value).append("\n");
            if(left!=null) {
                left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
            }
            return sb;
        }

        @Override
        public String toString() {
            return this.toString(new StringBuilder(), true, new StringBuilder()).toString();
        }
    }

    @Override
    public String toString() {
        return root.toString();
    }
}