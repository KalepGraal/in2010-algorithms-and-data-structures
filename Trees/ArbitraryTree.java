public class ArbitraryTree {
    Node root;

    int sumAll() {return sumAll(root);}

    int sumAll(Node n) {
        int sum = 0;
        for (Node child : n.children) {
            if (child != null) {
                sum += sumAll(child);
            }
        }
        return sum + n.data;
    }

    void depthAndHeight() {depthAndHeight(root, 0);}

    int depthAndHeight(Node n, int depth){
        n.depth = depth;
        int distToLeaf = 0;
        for (Node child : n.children){
            if (child == null) continue;
            int childHeight = depthAndHeight(child, depth+1);
            if (childHeight > distToLeaf) distToLeaf = childHeight;
        }
        n.height = distToLeaf;
        return distToLeaf + 1;
    }

    private class Node {
        int data;
        int depth; int height;

        Node[] children;
        Node(int data, int numChildren){
            this.data = data;
            children = new Node[numChildren];
        }
    }
}