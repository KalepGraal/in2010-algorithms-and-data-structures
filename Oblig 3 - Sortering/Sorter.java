import java.util.Arrays;

public interface Sorter {
    int[] sort(int[] A);
    String sortType();
}


// TODO remove comments, replace with own.
class SelectionSorter implements Sorter{
    @Override
    public String sortType() {
        return "SelectionSort";
    }

    @Override
    public int[] sort(int[] A) {
        for (int i = 0; i < A.length-1; i++) {
            //System.out.println(Arrays.toString(A));
            // Find the index, s, of the smallest element in A[i..n].
            int s = i;
            for (int j = i+1; j < A.length; j++)
                if (A[j] < A[s]) {
                    s = j;
                }
            if (i != s){
                // Swap A[i] and A[s]
                int t = A[s]; A[s] = A[i]; A[i] = t;
            }
        }
        /*System.out.println();*/
        return A;
    }
}

class InsertionSorter implements Sorter {
    @Override
    public String sortType() {
        return "InsertionSort";
    }

    @Override
    public int[] sort(int[] A) {
        for (int i = 1; i < A.length; i++) {
            //System.out.println(Arrays.toString(A));
            int x = A[i];
            int j = i;
            // Put x in the right place in A[1..i], moving larger elements up as needed.
            while (j > 0 && x < A[j-1]) {
                A[j] = A[j-1];
                j = j-1;
            }
            A[j] = x;
        }
        //System.out.println(Arrays.toString(A) + "\n");
        return A;
    }
}


// TODO, test both this one, and Correct.., compare how much the recursion stack affects runtime
class QuickSorter implements Sorter {
    @Override
    public String sortType() {
        return "quickSort";
    }

    @Override
    public int[] sort(int[] A) {
        int[] ret = inPlaceQuickSort(A, 0, A.length-1);
        //System.out.println();
        return ret;
    }

    private int[] inPlaceQuickSort(int[] A, int a, int b){
        if (a >= b) return A;
        //System.out.println(Arrays.toString(A));
        int l = CorrectQuickSorter.inPlacePartition(A, a, b);
        inPlaceQuickSort(A, a, l-1);
        inPlaceQuickSort(A, l+1, b);
        return A;
    }
}

class CorrectQuickSorter implements Sorter {

    @Override
    public String sortType() {
        return "CorrectQuickSort";
    }

    String sortType = "CorrectQuickSort";

    @Override
    public int[] sort(int[] A) {
        int[] ret = correctInPlaceQuickSort(A, 0, A.length-1);
        //System.out.println();
        return ret;
    }

    private int[] correctInPlaceQuickSort(int[] A, int a, int b){
        //System.out.println(Arrays.toString(A));
        while (a < b) {
            int l = inPlacePartition(A, a, b);
            if (l-a < b-l) {
                correctInPlaceQuickSort(A,a,l-1);
                a = l+1;
            } else {
                correctInPlaceQuickSort(A,l+1, b);
                b = l-1;
            }
        }
        return A;
    }

    static int inPlacePartition(int[] A, int a, int b){
        int p = A[b];   // the pivot
        int l = a;      // will scan rightward
        int r = b-1;    // will scan leftward
        while (l <= r) {
            while (l <= r && A[l] <= p) l++;
            while (r >= l && A[r] >= p) r--;
            if (l < r) {
                //swap A[l] and A[r]
                int t = A[l]; A[l] = A[r]; A[r] = t;
            }
        }
        // Swap A[l] and A[b]
        int t = A[l]; A[l] = A[b]; A[b] = t;
        return l;
    }
}


// TODO remake this one
// Only allows values from 0 to N-1,
// where N is number of elements in input array
class BucketSorter implements Sorter {

    @Override
    public String sortType() {
        return "bucketSort";
    }

    @Override
    public int[] sort(int[] A) {
        // En bucket for hver mulige verdi
        int[] bucket = new int[A.length];
        // Finner hvor mange elementer det er av hvert tall
        for (int i = 0; i < A.length; i++) {
            bucket[A[i]]++;
        }
        // k er neste index vi skal legge inn et element i arrayet
        for (int i = 0, k = 0; i < A.length; i++) {
            //System.out.println(Arrays.toString(A));
            // Legger inn i, dersom i var et element i A.
            for (int j = 0; j < bucket[i]; j++) {
                // Hver gang vi legger inn et nytt tall, øker vi k
                A[k++] = i;
            }
        }
        //System.out.println(Arrays.toString(A) + "\n");
        return A;
    }
}

class defaultSorter implements Sorter {
    @Override
    public String sortType() {
        return "Arrays.sort";
    }

    @Override
    public int[] sort(int[] A) {
        Arrays.sort(A);
        return A;
    }
}