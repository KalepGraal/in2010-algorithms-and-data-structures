import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class TestSorting {
    private static PrintWriter pw;

    public static void main(String[] args) {
        try {
            pw = new PrintWriter("output.csv");
            writeRuntimeToFile();


        } catch (FileNotFoundException ex) {
            System.out.println("Error writing to file");
            ex.printStackTrace();
            System.exit(1);
        } finally {
            pw.close();
        }

        //writeRuntimeToFile();
        /*testRuntime(desc,"desc");
        testRuntime(asc,"asc");
        testRuntime(c,"c");*/
    }

    private static void writeRuntimeToFile() {
        Sorter[] sorters = {
                new SelectionSorter(),  new InsertionSorter(),
                new QuickSorter(),      new CorrectQuickSorter(),
                new BucketSorter(),     new defaultSorter()
        };

        runtimeRandom(sorters);
        runtimeAsc(sorters);
        runtimeDesc(sorters);
    }

    private static final int maxN = 500000;

    private static void runtimeRandom(Sorter[] sorters) {
        List<int[]> arraysToSort = new LinkedList<>();
        System.out.println("runtime random");
        pw.println("Random");
        pw.print("Elements,");
        for (int i = 1000, multiplier = 2; i <= maxN; i*=multiplier){
            pw.print(i + ",");
            arraysToSort.add(createRandomIntArray(i));
            multiplier = multiplier == 2 ? 5 : 2;
        }
        pw.println();

        runSortTest(sorters, arraysToSort);
    }

    private static void runtimeAsc(Sorter[] sorters) {
        List<int[]> arraysToSort = new LinkedList<>();
        System.out.println("runtime asc");
        pw.println("\nAscending");
        pw.print("Elements,");
        for (int i = 1000, multiplier = 2; i <= maxN; i*=multiplier){
            pw.print(i + ",");
            arraysToSort.add(createAscIntArray(i));
            multiplier = multiplier == 2 ? 5 : 2;
        }
        pw.println();

        runSortTest(sorters, arraysToSort);
    }

    private static void runtimeDesc(Sorter[] sorters) {
        List<int[]> arraysToSort = new LinkedList<>();
        System.out.println("runtime desc");
        pw.println("\nDescending");
        pw.print("Elements,");
        for (int i = 1000, multiplier = 2; i <= maxN; i*=multiplier){
            pw.print(i + ",");
            arraysToSort.add(createDescIntArray(i));
            multiplier = multiplier == 2 ? 5 : 2;
        }
        pw.println();

        runSortTest(sorters, arraysToSort);
    }

    private static void runSortTest(Sorter[] sorters, List<int[]> arraysToSort) {
        for (Sorter sorter : sorters) {
            try {
                pw.print(sorter.sortType() + ",");
                for (int[] arr : arraysToSort){
                    testRuntime(arr.clone(), sorter);
                    System.out.println("finished: " + arr.length);
                }
            } catch (StackOverflowError e) {
                System.out.println(sorter.sortType() + " used too much memory");
            }
            pw.println();
        }
    }

    private static void testRuntime(int[] a, Sorter sorter){
        long msStart = System.nanoTime();
        sorter.sort(a);
        long time = System.nanoTime() - msStart;
        if (!isSorted(a)) throw new RuntimeException ("Array is not sorted!");


        pw.print(time + ",");
    }

    private static boolean isSorted(int[] a){
        int prev = a[0];
        for (int i : a) {
            if (i < prev) return false;
            prev = i;
        }
        return true;
    }


    // TODO output test results to .CSV

    private void testSort(Sorter sorter){
        int[] desc = createDescIntArray(10);
        int[] asc = createAscIntArray(10);
        int[] random = createRandomIntArray(10);

        Assert.assertTrue(sorter.sortType() + "ing for descending numbers 9-0",
                isSorted(sorter.sort(desc)));
        Assert.assertTrue(sorter.sortType() + "ing for ascending numbers 0-9",
                isSorted(sorter.sort(asc)));
        Assert.assertTrue(sorter.sortType() + "ing for random numbers",
                isSorted(sorter.sort(random)));
    }

    @Test
    public void testSelectionSort() {
        testSort(new SelectionSorter());
    }

    @Test
    public void testInsertionSort() {
        testSort(new InsertionSorter());
    }

    @Test
    public void testQuickSort() {
        testSort(new QuickSorter());
    }

    @Test
    public void testCorrectQuickSort() {
        testSort(new CorrectQuickSorter());
    }

    @Test
    public void testBucketSort() {
        testSort(new BucketSorter());
    }

    // Creates array with n random integers from 0 to n-1
    private static int[] createRandomIntArray(int n) {
        if (n < 0) throw new IllegalArgumentException("n must be positve");
        Random r = new Random();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt(n);
        }
        return arr;
    }

    // Creates array with integers from n-1 to 0
    private static int[] createDescIntArray(int n) {
        if (n < 0) throw new IllegalArgumentException("n must be positve");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = n-1-i;
        }
        return arr;
    }

    // Creates array with integers from 0 to n-1
    private static int[] createAscIntArray(int n) {
        if (n < 0) throw new IllegalArgumentException("n must be positve");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = i;
        }
        return arr;
    }

}