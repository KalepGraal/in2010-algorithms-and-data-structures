import java.util.ArrayList;
import java.util.List;

public class RedBlackBST<T extends Comparable<T>> implements BSTOper<T> {

    public static void main(String[] args) {
        BSTOper<Integer> bst = new RedBlackBST<>();
        //bst.addAll(1,2,3,4,5,6,7,8);
        bst.addAll(8,7,6,5,4,3,2,1);
        bst.remove(5);
        System.out.println(bst);
    }

    private Node root;

    @Override
    public void add(T value) {
        if(root == null) {
            root = new Node(value);
            root.setToBlack();
        }
        else root.add(value);
    }

    @Override
    public void addAll(T ... data) {
        for (T t : data) add(t);
    }

    @Override
    public boolean existsInTree(T value) {
        return root != null && root.existsInTree(value);
    }

    @Override
    public boolean remove(T value) {
        System.out.println("REMOVE not implementedby this tree");
        return false;
    }

    @Override
    public T findNearestSmallerThan(T value) {
        return root != null ? root.findNearestSmallerThan(value) : null;
    }

    @Override
    public int size() {
        return root != null ? root.size() : 0;
    }

    @Override
    public List<T> findInRange(T low, T high) {
        return root.findInRange(low, high);
    }

    @Override
    public List<T> sortedList() {
        return root.sortedList();
    }


    private static byte BLACK = 1;
    private static byte RED = 2;

    private class Node {
        Node left, right, parent;
        private byte colour = 0;
        T value;

        Node( T t ) { value = t;}

        boolean isRed() { return colour == RED; }
        boolean isBlack() { return colour == BLACK; }
        void setToRed() { colour = RED; }
        void setToBlack() { colour = BLACK; }

        // todo change
        private void add(T t){
            Node n = new Node(t);
            insertRecurse(root,n);
            insertRepairTree(n);

            root = n;
            while(root.parent != null){
                root = root.parent;
            }
        }

        void insertRecurse(Node root, Node n){
            if(root != null && n.value.compareTo(root.value) < 0){
                if(root.left != null) {
                    insertRecurse(root.left, n);
                    return;
                }
                else {
                    root.left = n;
                }
            } else if (root != null) {
                if (root.right != null){
                    insertRecurse(root.right, n);
                    return;
                } else
                    root.right = n;
            }

            n.parent = root;
            n.left = null;
            n.right = null;
            n.colour = RED;
        }

        void insertRepairTree(Node n){
            if(n.parent == null){
                n.setToBlack();
            } else if (n.parent.isBlack()){
                return;
            } else if (uncle(n) != null && uncle(n).isRed()){
                insertCase3(n);
            } else {
                insertCase4(n);
            }
        }

        void insertCase3(Node n){
            n.parent.setToBlack();
            uncle(n).setToBlack();
            grandparent(n).setToRed();
            insertRepairTree(grandparent(n));
        }

        void insertCase4(Node n){
            Node p = n.parent;
            Node g = grandparent(n);

            if(g != null){
                if (g.left != null && n == g.left.right){
                    p.rotateLeft();
                    n = n.left;
                } else if (g.right != null && n == g.right.left) {
                    p.rotateRight();
                    n = n.right;
                }
            }
            insertCase4Step2(n);
        }

        Node grandparent(Node n){
            return n.parent != null ? n.parent.parent : null;
        }

        Node uncle(Node n){
            if(n.parent == null)
                return null;
            return sibling(n.parent);
        }

        Node sibling(Node n){
            Node p = n.parent;
            if (p == null)
                return null;
            if (n == p.left)
                return p.right;
            else
                return p.left;
        }

        void insertCase4Step2(Node n){
            Node p = n.parent;
            Node g = grandparent(n);

            if(g != null){
                if (n == p.left)
                    g.rotateRight();
                else
                    g.rotateLeft();
                g.setToRed();
            }
            p.setToBlack();
        }

        private void rotateLeft(){
            Node nnew = right;
            Node p = parent;
            if (nnew == null) return;
            right = nnew.left;
            nnew.left = this;
            parent = nnew;
            if(right != null) {
                right.parent = this;
            }
            if(p != null){
                if(this == p.left) {
                    p.left = nnew;
                }
                else if (this == p.right) {
                    p.right = nnew;
                }
            }
            nnew.parent = p;
        }

        private void rotateRight() {
            Node nnew = left;
            Node p = parent;
            if(nnew == null) return;
            left = nnew.right;
            nnew.right = this;
            parent = nnew;
            if(left != null)
                left.parent = this;
            if(p != null){
                if(this == p.left){
                    p.left = nnew;
                }
                else if (this == p.right){
                    p.right = nnew;
                }
            }
            nnew.parent = p;
        }

        // todo change
        private boolean remove(T t, Node prevNode){
            /* Ikke en del av ekstra-oppgaven på obligen */
            return false;
        }

        private int size(){
            int size = 0;
            if (left != null) size += left.size();
            if (right != null) size += right.size();
            return size + 1;
        }

        private T min(){
            if(left != null) return left.min();
            return value;
        }

        private T max(){
            if(right != null) return right.max();
            return value;
        }

        private boolean existsInTree(T t){
            if (t.compareTo(this.value) == 0) return true;
            if (t.compareTo(this.value) < 0) return left != null && left.existsInTree(t);
            return right != null && right.existsInTree(t);
        }

        private List<T> sortedList() {
            List<T> l = new ArrayList<>();
            if(left != null) l.addAll(left.sortedList());
            l.add(this.value);
            if(right != null) l.addAll(right.sortedList());
            return l;
        }

        private T findNearestSmallerThan(T t){
            if (right == null && left == null)
                return value.compareTo(t) < 0 ? value : null;

            if (value.compareTo(t) < 0) {
                T r = right.findNearestSmallerThan(t);
                if (r == null) return value;
                return value.compareTo(r) < 0 ? r : value;
            }
            else {
                if (left != null)
                    return left.findNearestSmallerThan(t);
                return null;
            }
        }

        private List<T> findInRange(T low, T high){
            List<T> l = new ArrayList<>();
            if (left != null && value.compareTo(low) >= 0)
                l.addAll(left.findInRange(low,high));
            if (value.compareTo(low) >= 0 && value.compareTo(high) <= 0)
                l.add(value);
            if (right != null && value.compareTo(high) <= 0)
                l.addAll(right.findInRange(low,high));
            return l;
        }

        // brukes til utskrift/debugging
        StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
            if(right!=null) {
                right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
            }
            String stringToAppend = isRed() ? "\u001B[31m" + value + "\u001B[0m" : ""+value;
            sb.append(prefix).append(isTail ? "└── " : "┌── ").append(stringToAppend).append("\n");
            if(left!=null) {
                left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
            }
            return sb;
        }
    }

    @Override
    public String toString() {
        return root != null ? root.toString(new StringBuilder(), true, new StringBuilder()).toString() : "Empty";
    }

    private Node findParent( Node n ){
        if (root == null) return null;
        Node i = root;
        Node parent = null;
        while (i != null){
            if (i == n) return parent;
            parent = i;
            if (n.value.compareTo(i.value) < 0) i = i.left;
            else i = i.right;
        }
        return null;
    }
    private Node findGrandparent( Node n ){ return findParent(findParent(n)); }
    private Node find( T value ){
        Node n = root;
        while (n != null){
            if (value.compareTo(n.value) == 0) return n;
            if (value.compareTo(n.value) < 0) n = n.left;
            else n = n.right;
        }
        return null;
    }
}