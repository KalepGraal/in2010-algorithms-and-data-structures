import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BSTree<T extends Comparable<T>> implements BSTOper<T> {

    private Node root;

    @Override
    public void add(T value) {
        if(root == null) root = new Node(value);
        else root.add(value);
    }

    @Override
    public void addAll(T ... data) {
        for (T t : data) add(t);
    }

    @Override
    public boolean existsInTree(T value) {
        return root != null && root.existsInTree(value);
    }

    @Override
    public boolean remove(T value) {
        return root != null && root.remove(value, root);
    }

    @Override
    public T findNearestSmallerThan(T value) {
        return root != null ? root.findNearestSmallerThan(value) : null;
    }

    @Override
    public int size() {
        return root != null ? root.size() : 0;
    }

    @Override
    public List<T> findInRange(T low, T high) {
        return root.findInRange(low, high);
    }

    @Override
    public List<T> sortedList() {
        return root.sortedList();
    }

    private class Node {
        Node left, right;
        T value;
        Node( T t ) { value = t; }

        private void add(T t){
            if (t.compareTo(value) < 0){
                if (left != null) left.add(t);
                else left = new Node(t);
            } else {
                if (right != null) right.add(t);
                else right = new Node(t);
            }
        }

        private boolean remove(T t, Node prevNode){
            if(t.compareTo(value) == 0) {
                if(root == this && left == null && right == null) {
                    root = null;
                    return true;
                }
                if(right != null){
                    value = right.min();
                    return right.remove(value, this);
                } else if (left != null){
                    value = left.max();
                    return left.remove(value, this);
                } else {
                    if (prevNode.right == this) {
                        prevNode.right = null;
                        return true;
                    }
                    else if (prevNode.left == this) {
                        prevNode.left = null;
                        return true;
                    }
                }
            }
            else if(t.compareTo(this.value) > 0 && right != null)
                return right.remove(t, this);
            else if(t.compareTo(value) < 0 && left != null)
                return left.remove(t, this);
            return false;
        }

        private int size(){
            int size = 0;
            if (left != null) size += left.size();
            if (right != null) size += right.size();
            return size + 1;
        }

        private T min(){
            if(left != null) return left.min();
            return value;
        }

        private T max(){
            if(right != null) return right.max();
            return value;
        }

        private boolean existsInTree(T t){
            if (t.compareTo(this.value) == 0) return true;
            if (t.compareTo(this.value) < 0) return left != null && left.existsInTree(t);
            return right != null && right.existsInTree(t);
        }

        private List<T> sortedList() {
            List<T> l = new ArrayList<>();
            if(left != null) l.addAll(left.sortedList());
            l.add(this.value);
            if(right != null) l.addAll(right.sortedList());
            return l;
        }

        private T findNearestSmallerThan(T t){
            if (right == null && left == null)
                return value.compareTo(t) < 0 ? value : null;

            if (value.compareTo(t) < 0) {
                T r = right.findNearestSmallerThan(t);
                if (r == null) return value;
                return value.compareTo(r) < 0 ? r : value;
            }
            else {
                if (left != null)
                    return left.findNearestSmallerThan(t);
                return null;
            }
        }

        private List<T> findInRange(T low, T high){
            List<T> l = new ArrayList<>();
            if (left != null && value.compareTo(low) >= 0)
                l.addAll(left.findInRange(low,high));
            if (value.compareTo(low) >= 0 && value.compareTo(high) <= 0)
                l.add(value);
            if (right != null && value.compareTo(high) <= 0)
                l.addAll(right.findInRange(low,high));
            return l;
        }

        // brukes til utskrift/debugging
        StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
            if(right!=null) {
                right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
            }
            sb.append(prefix).append(isTail ? "└── " : "┌── ").append(value).append("\n");
            if(left!=null) {
                left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
            }
            return sb;
        }
    }

    @Override
    public String toString() {
        return root != null ? root.toString(new StringBuilder(), true, new StringBuilder()).toString() : "Empty";
    }

    private Node findParent( Node n ){
        if (root == null) return null;
        Node i = root;
        Node parent = null;
        while (i != null){
            if (i == n) return parent;
            parent = i;
            if (n.value.compareTo(i.value) < 0) i = i.left;
            else i = i.right;
        }
        return null;
    }
    private Node findGrandparent( Node n ){ return findParent(findParent(n)); }
    private Node find( T value ){
        Node n = root;
        while (n != null){
            if (value.compareTo(n.value) == 0) return n;
            if (value.compareTo(n.value) < 0) n = n.left;
            else n = n.right;
        }
        return null;
    }
}