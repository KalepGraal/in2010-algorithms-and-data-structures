import java.util.ArrayList;
import java.util.List;

class BSTtest {

    public static List<Integer> tallrekke( int antall ) {
        java.util.Random tilf = new java.util.Random(20102018);
        List<Integer> tall = new ArrayList<>();
        for ( int i=0; i<antall; i++) {
            tall.add(tilf.nextInt());
        }
        return tall;
    }
    

    public static void main(String[] args) {

        int antall = Integer.parseInt(args[0]);

        List<Integer> intarr = tallrekke(antall);
        BSTree testtre = new BSTree();

        if (antall == 0 ) {
	    
	    System.out.println("n     antall     høyde");
            for (int ant=1; ant <= 100000000; ant=ant*10) {
                intarr = tallrekke(ant);
                for (int i : intarr) { testtre.add(i); }
                System.out.print(ant);
                System.out.print(" " + testtre.size());
                //System.out.println(" " + testtre.height());
            }

        }
        else {
            
            for (int i : intarr) { testtre.add(i); }
            //intarr = testtre.preorderArray();
            //for (int i : intarr) { System.out.println(i); }
            intarr = testtre.sortedList();
            for (int i : intarr) { System.out.println(i); }
        }
    }
}
