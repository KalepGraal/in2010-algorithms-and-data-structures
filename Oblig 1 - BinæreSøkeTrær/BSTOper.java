import java.util.List;

interface BSTOper <T> {
    void add( T value );
    boolean remove( T value );
    int size();
    boolean existsInTree( T value );
    T findNearestSmallerThan( T value );
    void addAll( T ... values );
    List<T> sortedList() ; // inorder
    List<T> findInRange (T low, T high);
}