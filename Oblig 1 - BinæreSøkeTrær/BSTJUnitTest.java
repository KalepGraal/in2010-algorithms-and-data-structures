import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BSTJUnitTest {

    private static Integer[] ints = { 6, 4, 8, 5, 1, 9, 7, 11, 2 };

    @Test
    public void testAddAndExistsInTree(){
        BSTOper<Integer> bst = new BSTree<>();
        for (Integer i : ints)
            bst.add(i);
        for (Integer i : ints)
            assertTrue("Element " + i  + " not in tree", bst.existsInTree(i));
    }

    @Test
    public void testAddAll(){
        BSTOper<Integer> bst = new BSTree<>();
        Integer[] ints = { 6, 4, 8, 5, 1, 9, 7, 11, 2 };
        bst.addAll(ints);
        for (Integer i : ints)
            assertTrue("Element " + i  + " not in tree", bst.existsInTree(i));
    }

    @Test
    public void testSize(){
        BSTOper<Integer> bst = new BSTree<>();
        assertEquals(0, bst.size());
        bst.add(3);
        assertEquals(1, bst.size());
        Integer[] ints = { 6, 4, 8, 5, 1, 9, 7, 11, 2 };
        bst.addAll(ints);
        assertEquals(10, bst.size());
    }

    private void testRemove(BSTOper<Integer> bst, int x){
        bst.remove(x);
        assertFalse(bst.existsInTree(x));
    }

    @Test
    public void testRemove(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.addAll(ints);

        testRemove(bst,8);
        assertTrue(bst.existsInTree(7));
        assertTrue(bst.existsInTree(9));
        assertTrue(bst.existsInTree(6));

        testRemove(bst, 4);
        assertTrue(bst.existsInTree(5));
        assertTrue(bst.existsInTree(1));
        assertTrue(bst.existsInTree(2));
    }

    @Test
    public void testRemoveRoot(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.add(3);
        testRemove(bst,3);

        bst.addAll(ints);
        testRemove(bst,6);
        for (int i = 1; i < ints.length; i++)
            assertEquals(true, bst.existsInTree(ints[i]));
    }

    @Test
    public void testRemoveLeaf(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.addAll(ints);
        testRemove(bst, 11);
        testRemove(bst, 2);
        testRemove(bst, 1);
    }

    @Test
    public void testFindNearestSmallerThan(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.addAll(ints);

        assertEquals(new Integer(7), bst.findNearestSmallerThan(8));
        assertEquals(new Integer(5), bst.findNearestSmallerThan(6));
        assertEquals(new Integer(8), bst.findNearestSmallerThan(9));
        assertEquals(new Integer(4), bst.findNearestSmallerThan(5));
        assertEquals(null, bst.findNearestSmallerThan(1));
    }

    @Test
    public void testSortedList(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.addAll(ints);
        Integer[] sorted = {1, 2, 4, 5, 6, 7, 8, 9, 11};
        List<Integer> sList = bst.sortedList();
        for (int i = 0; i < sorted.length; i++){
            assertEquals(sorted[i], sList.get(i));
        }
    }

    @Test
    public void testFindInRange(){
        BSTOper<Integer> bst = new BSTree<>();
        bst.addAll(ints);

        List<Integer> range = bst.findInRange(4,8);
        for (int i : ints){
            if(i >= 4 && i <= 8) assertTrue(range.contains(i));
        }

        range = bst.findInRange(0,4);
        for (int i : ints){
            if(i >= 0 && i <= 4) assertTrue(range.contains(i));
        }

        range = bst.findInRange(0,12);
        for (int i : ints){
            if(i >= 0 && i <= 12) assertTrue(range.contains(i));
        }
    }
}