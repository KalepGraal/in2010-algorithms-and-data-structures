Programmet skal kompileres med javac (standard verdier)
Ligger en main i RedBlackBST.java, som hovedsaklig bruktes til testing.

Endret på interfacet for å bedre støtte generiske typer.
(Endret alle int til T, endret int[] til List<T>, ved unntak av addAll, hvor T ... ble benyttet.
Endret navn på sortedArray til sortedList.)

Ettersom jeg lagde en generisk løsning, valgte jeg å la treet legge inn verdier selv om det finnes et object i treet
med samme verdi.

Fant en toString på nettet, som jeg brukte til debugging.

Lagde en JUnit4 test, for å enklere kunne teste trærne. (Denne tester ikke alle tilfeller)

På det rødsvarte treet implementerte jeg stort sett fremgangsmåten som ligger på wikipedia:
https://en.wikipedia.org/wiki/Red%E2%80%93black_tree

Vil helst ha tilbakemelding på hva som kunne vært gjort mer effektivt i BSTree (ikke så farlig med RedBlackBST).